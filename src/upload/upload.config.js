(function () {

  'use strict';
  
  angular
    .module('app.upload')
    .config(Configuration);

  require('./upload.controller');
  
  Configuration.$inject = ['$routeProvider'];
  
  function Configuration($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: '/upload/upload.view.html',
      controller: 'UploadController',
      controllerAs: 'upload'
    });
  }

})();
